﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumbingStoreSupply
{
    public class ContractItem
    {
        public ContractItem() { }

        public ContractItem(Product product, int quantity, Employee person)
        {
            Product = product;
            Quantity = quantity;
        }

        public Product Product { get; set; }



        public int Quantity { get; set; }

        public override string ToString()
        {
            return $"{Quantity} {Product.Name}";
        }
    }
}
