﻿using Robot.Common;
using System.Collections.Generic;
using System.Linq;
using static VarvarchukIvan.RobotChallange.DistanceHelper;

namespace VarvarchukIvan.RobotChallange
{
    public class VarvarchukIvanAlgorithm : IRobotAlgorithm
    {
        public string Author => "Ivan Varvarchuk";

        public static int RoundNumber { get; set; } = 0;


        public VarvarchukIvanAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundNumber++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            
            Position stationPosition =  FindOptimalStation(movingRobot, map)??
                FindNearestFreeStation(movingRobot, map, robots)??
                FindNearestOccupiedStation(movingRobot, map, robots);


            if (stationPosition != null)
            {

                if (movingRobot.Position.IsInSameSector(stationPosition))
                {
                    if ((movingRobot.Energy > 400) && (robots.Count(r => r.OwnerName == "Ivan Varvarchuk") < 67 && RoundNumber < 40)
                        && FindEnemiesAround(movingRobot.Position, robots).Count < 2)
                    {
                        var newRobot = new CreateNewRobotCommand();
                        newRobot.NewRobotEnergy = 100;
                        return newRobot;

                    }

                    return new CollectEnergyCommand();
                }
                else
                {
                    return new MoveCommand() { NewPosition = CalculateHop(movingRobot, stationPosition) };
                }
            }
            else
            {
                if (RoundNumber > 30 && FindRichestEnemy(movingRobot, map, robots) != null)
                {
                    Position target = FindRichestEnemy(movingRobot, map, robots);
                    return new MoveCommand() { NewPosition = target };
                    
                }
                else
                {
                    return new MoveCommand { NewPosition = movingRobot.Position };
                }
            }
        }

    }
}
