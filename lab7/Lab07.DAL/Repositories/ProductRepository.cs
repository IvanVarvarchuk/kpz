﻿using Lab07.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lab07.DAL.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private PlumbingSupplyContext _context;

        public ProductRepository(PlumbingSupplyContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            var products = await _context.Products.Include(x => x.Category).AsNoTracking().ToListAsync();
            return products;
        }

        public async Task<Product> GetAsync(int id)
        {
            var product = await _context.Products.Include(x => x.Category).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return product;
        }

        public async Task<Product> AddAsync(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();

            return product;
        }

        public async Task<Product> UpdateAsync(Product product)
        {
            var updatedProduct = await _context.Products.FirstOrDefaultAsync(x => x.Id == product.Id);
            updatedProduct.Price = product.Price;
            updatedProduct.Name = product.Name;
            updatedProduct.Category.Name = product.Category.Name;
            updatedProduct.Category.Id = product.Category.Id;
            updatedProduct.CategoryId = product.CategoryId;

            await _context.SaveChangesAsync();
            return updatedProduct;
        }

        public async Task DeleteAsync(int id)
        {
            var deletedProduct = await _context.Products.FirstOrDefaultAsync(x => x.Id == id);
            _context.Products.Remove(deletedProduct);
            await _context.SaveChangesAsync();

        }
    }
}
