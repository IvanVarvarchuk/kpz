﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace PlumbingSupplyWPF.Converters
{
    public class ImageConverter : IValueConverter
    {
        private Dictionary<string, BitmapImage> cash = new Dictionary<string, BitmapImage>();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string type = value.ToString();
            if (!cash.ContainsKey(type))
            {
                Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                var uri = new Uri(string.Format("../Resourses/Images/{0}/{1}", type, parameter), UriKind.Relative);
                cash.Add(type, new BitmapImage(uri));
            }
            return cash[type];

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
