﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lab07.DAL.Models
{
    public partial class Product
    {
        public Product()
        {
            ContractItems = new HashSet<ContractItem>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int? CategoryId { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<ContractItem> ContractItems { get; set; }
    }
}
