﻿using AutoMapper;
using Lab07.DAL.Models;
using Lab07.DAL.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab07_WebAPI.Configurations
{
    public static class ServicesExtention
    {
        public static void RegisterMapperProfiles(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(c =>
            {
                c.AddProfile<MapperProfile>();
            });

            services.AddTransient(s => configuration.CreateMapper());
        }

        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IRepository<Category>, CategoryRepository>();
            services.AddTransient<IRepository<Product>, ProductRepository>();
            services.AddTransient<IRepository<Contract>, ContractRepository>();
            services.AddTransient<IRepository<ContractItem>, ContractItemRepository>();

        }
    }
}
