﻿using AutoMapper;
using Lab06.DomainLogic;
using Lab06.DomainLogic.Models;
using Lab06_ADO.Net.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Lab06_ADO.Net
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Category, CategoryViewModel>();
                cfg.CreateMap<Contract, ContractViewModel>();
                cfg.CreateMap<ContractItem, ContractItemViewModel>();
                cfg.CreateMap<Product, ProductViewModel>();
                cfg.CreateMap<ContractViewModel, Contract>();
                cfg.CreateMap<ContractItemViewModel, ContractItem>();
                cfg.CreateMap<CategoryViewModel, Category>();
                cfg.CreateMap<ProductViewModel, Product>();
            }));
            DomainContext context =
                new DomainContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            DataViewModel viewModel = new DataViewModel(context, mapper);


            MainWindow window = new MainWindow() { DataContext = viewModel };
            window.Show();
        }
    }
}
