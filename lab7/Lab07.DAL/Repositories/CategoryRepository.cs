﻿using Lab07.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lab07.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {

        private PlumbingSupplyContext _context;
        public CategoryRepository(PlumbingSupplyContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            var categories = await _context.Categories.Include(x => x.Products).AsNoTracking().ToListAsync();
            return categories;
        }

        public async Task<Category> GetAsync(int id)
        {
            var category = await _context.Categories.Include(x => x.Products).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return category;
        }

        public async Task<Category> AddAsync(Category category)
        {
            await _context.Categories.AddAsync(category);
            await _context.SaveChangesAsync();

            return category;
        }

        public async Task<Category> UpdateAsync( Category category)
        {
            var updatedCategory = await _context.Categories.FirstOrDefaultAsync(x => x.Id == category.Id);

            updatedCategory.Name = category.Name;

            await _context.SaveChangesAsync();
            return updatedCategory;
        }

        public async Task DeleteAsync(int id)
        {
            var deletedCategory = await _context.Categories.FirstOrDefaultAsync(x => x.Id == id);
            _context.Categories.Remove(deletedCategory);
            await _context.SaveChangesAsync();
        }

        
    }
}
