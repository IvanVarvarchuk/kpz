﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupply.Domain.Models;
using PlumbingSupply.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PlumbingSupplyWPF.BLL.Services
{
    public class ContractService : IDataService<ContractDTO>
    {
        private readonly IMapper _mapper;
        private PlumbingSupplyDBContextFactory _contextFactory;

        public ContractService(PlumbingSupplyDBContextFactory contextFactory = null)
        {
            _contextFactory = contextFactory?? new PlumbingSupplyDBContextFactory();
            _mapper = new Mapper(new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<Contract, ContractDTO>();
                cfg.CreateMap<ContractDTO, Contract>();
                
                cfg.CreateMap<ContractItem, ContractItemDTO>();
                cfg.CreateMap<ContractItemDTO, ContractItem>();

                cfg.CreateMap<Location, LocationDTO>();
                cfg.CreateMap<LocationDTO, Location>();

                cfg.CreateMap<Product, ProductDTO>();
                cfg.CreateMap<ProductDTO, Product>();

                cfg.CreateMap<Supplier, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, Supplier>();

                cfg.CreateMap<Warehouse, WarehouseDTO>();
                cfg.CreateMap<WarehouseDTO, Warehouse>();
            }));
        }

        public void Create(ContractDTO entity)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Contracts.Add(_mapper.Map<Contract>(entity));
                context.SaveChanges();
            }
        }

        public void Delete(ContractDTO entity)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                Contract toRemove = _mapper.Map<Contract>(entity);
                context.Contracts.Remove(context.Contracts.First(x => x.Id == toRemove.Id));
                context.SaveChanges();
            }
        }

        public IEnumerable<ContractDTO> GetAll()
        {
            List<ContractDTO> res = new List<ContractDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                List<Contract> contracts = context.Contracts
                    .Include(x=>x.Warehose).ThenInclude(x=>x.Address)
                    .Include(x => x.Items).ThenInclude(x => x.Product)
                    .ToList();
                foreach (var item in contracts)
                {
                    res.Add(_mapper.Map<ContractDTO>(item));
                }
            }
            return res;
        }

        public void Update(int id, ContractDTO entity)
        {
            Contract toUpdate = _mapper.Map<Contract>(entity);
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Entry(context.Contracts.First(x => x.Id == entity.Id)).CurrentValues.SetValues(toUpdate);
                context.SaveChanges();
            }
        }
    }
}
