﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04_Classes.TestPolymorphism
{
    public interface IBase
    {
        int DoSmth(object o);
    }
    public class First : IBase
    {
        public int DoSmth(object i)
        {
            return 1;
        }
    }
    public class Second : IBase
    {
        public int DoSmth(object s)
        {
            return 2;
        }
    }
}
