﻿using PlumbingStoreSupply;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab02_Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Warehouse warehouse = new Warehouse() { Address = "Ternopil, Podilska street 21" };
            List<ContractItem> contractItems = new List<ContractItem>
            {
                new ContractItem(){ Product = new Product {CategoryID = 002, Name = "Product1", Price= 320, SupplierID = 001}, Quantity = 2},
                new ContractItem(){ Product = new Product {CategoryID = 003, Name = "Product2", Price= 2320, SupplierID = 004},Quantity = 3},
                new ContractItem(){ Product = new Product {CategoryID = 001, Name = "Product3", Price= 190, SupplierID = 001}, Quantity = 12},
                new ContractItem(){ Product = new Product {CategoryID = 002, Name = "Product4", Price= 30, SupplierID = 001},  Quantity = 70},
                new ContractItem(){ Product = new Product {CategoryID = 003, Name = "Product5", Price= 720, SupplierID = 006}, Quantity = 4},
                new ContractItem(){ Product = new Product {CategoryID = 001, Name = "Product6", Price= 504, SupplierID = 003}, Quantity = 6},
                new ContractItem(){ Product = new Product {CategoryID = 002, Name = "Product7", Price= 3110, SupplierID = 005},Quantity = 1},
                new ContractItem(){ Product = new Product {CategoryID = 002, Name = "Product8", Price= 230, SupplierID = 002}, Quantity = 6},

            };
            
           
            warehouse.Notify += DisplayMessage;

            foreach (var item in contractItems)
            {
                warehouse.Put(item);
            }

            warehouse.Take(contractItems[4]);
            warehouse.Take(contractItems[4]);
            warehouse.Notify -= DisplayMessage;
            Console.ReadKey();
        }
    

        

        private static void DisplayMessage(object sender, WarehouseEventArgs e)
        {
            Console.WriteLine($"Target Product: {e.Item}");
            Console.WriteLine(e.Message);
        }


    }
}
