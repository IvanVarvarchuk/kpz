﻿using System.Collections.Generic;
using System.Linq;

namespace LinqTests
{
    public static class Exention
    {

        public static bool IsOrdered<T>(this IEnumerable<T> sequence, Comparer<T> comparer = null)
        {
            if (comparer == null)
            {
                comparer = Comparer<T>.Default;
            }
            return sequence.Zip(sequence.Skip(1),
                (curr, next) => comparer.Compare(curr, next) <= 0)
               .All(x => x);
        }
        
        public static bool IsOrderedDescending<T>(this IEnumerable<T> sequence)
        {
            return sequence.Reverse().IsOrdered();
        }
    }
}
