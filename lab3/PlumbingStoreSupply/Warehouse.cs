﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumbingStoreSupply
{
    public class WarehouseEventArgs
    {
        public string Message { get; }
     
        public ContractItem Item { get; }

        public WarehouseEventArgs(string mes, ContractItem item)
        {
            Message = mes;
            Item = item;
        }
    }

    public class Warehouse
    {
        #region Constructors
        public Warehouse(): this(String.Empty) { }

        public Warehouse(string address) : this(address, new List<ContractItem>()) { }

        public Warehouse(string address, List<ContractItem> products)
        {
            Address = address;
            Products = products;
        }

        #endregion

        #region Properties
        public List<ContractItem> Products { get; set; }

        public string Address { get; set; }

        #endregion

        public delegate void AccountHandler(object sender, WarehouseEventArgs e);

        public event AccountHandler Notify;


        public void Put(ContractItem item)
        {
            if (Products.Contains(item)) { Products.Find(i => i.Product == item.Product).Quantity += item.Quantity; }
            Products.Add(item);
            Notify?.Invoke(this, 
                new WarehouseEventArgs($"Delivered to {Address} {item.Quantity}  {item.Product.Name}", item));
        }
        public void Take(ContractItem item)
        {
            if (Products.Contains(item) && Products.Find(i => i.Product == item.Product).Quantity <= item.Quantity)
            {
                Products.Remove(item);
                Notify?.Invoke(this, 
                 new WarehouseEventArgs($"Removed from {Address} {item.Quantity}  {item.Product.Name}", item));
            }
            else
            {
                Notify?.Invoke(this, 
                 new WarehouseEventArgs($"There in no {item.Quantity}  {item.Product.Name} on {Address} ", item)); ;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() + ((this.Address.Length << 2) * this.Products.Count);
        }

        public static implicit operator Warehouse(string str) => new Warehouse(str);

        public static explicit operator string(Warehouse warehouse) => warehouse.ToString();

        public override string ToString()
        {
            StringBuilder warehouseStr = new StringBuilder();
            warehouseStr.Append($"Address:{Address}\n Products");
            foreach (var prod in Products)
                warehouseStr.Append(prod.ToString()).Append(" \n");
            return warehouseStr.ToString();
        }
    }
}
