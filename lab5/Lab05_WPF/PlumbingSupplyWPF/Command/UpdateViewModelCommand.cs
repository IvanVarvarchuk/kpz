﻿using PlumbingSupplyWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace PlumbingSupplyWPF.Command
{
    public enum ViewModelType 
    {
        Products,
        Suppliers,
        Deliveries,
        Contracts
    }

    public class UpdateViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private MainViewModel mainViewModel;

        public UpdateViewModelCommand(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter.ToString() == ViewModelType.Products.ToString())
            {
                mainViewModel.CurrentViewModel = new DataViewModel();
            }
            if (parameter.ToString() ==  ViewModelType.Suppliers.ToString())
            {
                mainViewModel.CurrentViewModel = new SuppliersViewModel();
            }
            if (parameter.ToString() ==  ViewModelType.Contracts.ToString())
            {
                mainViewModel.CurrentViewModel = new ContractsViewModel();
            }
            if (parameter.ToString() ==  ViewModelType.Deliveries.ToString())
            {
                mainViewModel.CurrentViewModel = new DeliveriesViewModel();
            }

        }
    }
}
