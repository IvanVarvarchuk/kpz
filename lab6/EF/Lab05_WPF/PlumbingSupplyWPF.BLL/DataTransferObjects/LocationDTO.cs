﻿namespace PlumbingSupply.BLL.DataTransferObjects
{
    public class LocationDTO
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }

    }
}