﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlumbingSupplyWPF.Controls
{
    /// <summary>
    /// Interaction logic for SupplierCard.xaml
    /// </summary>
    public partial class SupplierCard : UserControl
    {
        public SupplierCard()
        {
            InitializeComponent();
        }

        public ImageSource LogoSource
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.RegisterAttached("LogoSource", typeof(ImageSource), typeof(SupplierCard), null);

        public string ButtonText
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(SupplierCard), null);
    }
}
