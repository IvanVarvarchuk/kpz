﻿using AutoMapper;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupplyWPF.BLL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class SuppliersViewModel : ViewModelBase
    {
        private IMapper _mapper;
        private DataService _model;

        public SuppliersViewModel(DataService dataService = null)
        {
            _model = dataService ?? new DataService();
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CategoryViewModel, CategoryDTO>();
                cfg.CreateMap<CategoryDTO, CategoryViewModel>();
                                
                cfg.CreateMap<DeliveryViewModel, DeliveryDTO>();
                cfg.CreateMap<DeliveryDTO, DeliveryViewModel>();

                cfg.CreateMap<LocationViewModel, LocationDTO>();
                cfg.CreateMap<LocationDTO, LocationViewModel>();

                cfg.CreateMap<ProductViewModel, ProductDTO>();
                cfg.CreateMap<ProductDTO, ProductViewModel>();

                cfg.CreateMap<SupplierViewModel, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, SupplierViewModel>();
            }));
            Products = new ObservableCollection<ProductViewModel>();
            Categories = new ObservableCollection<CategoryViewModel>();
            Suppliers = new ObservableCollection<SupplierViewModel>();
            LoadData();

        }

        private ObservableCollection<ProductViewModel> _products;
        public ObservableCollection<ProductViewModel> Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
               
                OnPropertyChanged(nameof(Products));
            }
        }

        private ObservableCollection<CategoryViewModel> _categories;
        public ObservableCollection<CategoryViewModel> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        private ObservableCollection<SupplierViewModel> _suppliers;
        public ObservableCollection<SupplierViewModel> Suppliers
        {
            get
            {
                return _suppliers;
            }
            set
            {
                _suppliers = value;
                OnPropertyChanged(nameof(Suppliers));
            }
        }

        private void LoadData()
        {
            foreach (var item in _model.GetAllProducts())
            {
                Products.Add(_mapper.Map<ProductViewModel>(item));
            }
            

            foreach (var item in _model.GetAllSuppliers())
            {
                Suppliers.Add(_mapper.Map<SupplierViewModel>(item));
            }

        }

    }
}
