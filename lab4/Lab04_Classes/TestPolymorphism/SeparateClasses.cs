﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04_Classes.TestPolymorphism
{
    public class A
    {
        public int DoSmth(int i)
        {
            return 1;
        }
    }
    public class B
    {
        public int DoSmth(string s)
        {
            return 3;
        }

    }
}
