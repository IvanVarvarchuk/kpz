﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04_Classes.TestPolymorphism
{
    public class BaseClass
    {
        public virtual int DoSmth(object o)
        {
            return 0;
        }

    }
    public class AClass : BaseClass
    {
        public override int DoSmth(object s)
        {
            return 1;
        }
    }

    public class BClass : BaseClass
    {
        public override int DoSmth(object i)
        {
            return 3;
        }

    }
}

