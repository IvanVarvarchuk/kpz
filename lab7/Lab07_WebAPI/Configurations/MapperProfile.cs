﻿using AutoMapper;
using Lab07.DAL.Models;
using Lab07_WebAPI.ViewModels;
using Lab07_WebAPI.ViewModels.Category;
using Lab07_WebAPI.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab07_WebAPI.Configurations
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Product, ProductViewModel>();
            CreateMap<ProductViewModel, Product>();

            CreateMap<Product, CreateProductViewModel>();
            CreateMap<CreateProductViewModel, Product>();

            CreateMap<Product, UpdateProductViewModel>();
            CreateMap<UpdateProductViewModel, Product>();


            CreateMap<Category, CategoryViewModel>();
            CreateMap<CategoryViewModel, Category>();

            CreateMap<Category, CreateCategoryViewModel>();
            CreateMap<CreateCategoryViewModel, Category>();

            CreateMap<Category, UpdateCategoryViewModel>();
            CreateMap<UpdateCategoryViewModel, Category>();

            CreateMap<Contract, ContractItemViewModel>();
            CreateMap<ContractItemViewModel, Contract>();



        }
    }
}
