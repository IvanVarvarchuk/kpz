﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab06_ADO.Net.ViewModels
{
    public class ContractItemViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private int _quantity;
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
                OnPropertyChanged(nameof(Quantity));
            }
        }

        private int _contracId;
        public int ContractId
        {
            get
            {
                return _contracId;
            }
            set
            {
                _contracId = value;
                OnPropertyChanged(nameof(ContractId));
            }
        }

        private ProductViewModel productViewModel;
        public ProductViewModel Product
        {
            get
            {
                return productViewModel;
            }
            set
            {
                productViewModel = value;
                OnPropertyChanged(nameof(Product));
            }
        }

    }
}
