﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04_Classes.TestPolymorphism
{
    public class ReflectionClass
    {
        public int DoSmth(object o)
        {
            Type myType = o.GetType();
            //Type myType = Type.GetType(T, false, true);

            if (myType.Name == "string")
                return 1;
            if (myType.Name == "int")
                return 2;
            else
                return 0;
        }

    }
}
