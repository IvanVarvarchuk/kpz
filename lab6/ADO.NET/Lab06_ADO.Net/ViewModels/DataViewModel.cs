﻿using AutoMapper;
using Lab06.DomainLogic;
using Lab06.DomainLogic.Models;
using Lab06_ADO.Net.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Lab06_ADO.Net.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        private DomainContext _domainContext;
        private IMapper _mapper;
        private ObservableCollection<CategoryViewModel> _categories;
        private ObservableCollection<ContractViewModel> _contracts;
        private ObservableCollection<ProductViewModel> _products;
        private ProductViewModel _selectedProduct;

        public DataViewModel(DomainContext domainContext, IMapper mapper)
        {
            _domainContext = domainContext;
            _mapper = mapper;

            LoaderData();
        }

        private void LoaderData()
        {
            _newProducts = new ObservableCollection<ProductViewModel>();
            Categories = _mapper.Map<ObservableCollection<CategoryViewModel>>(_domainContext.GetCategories());
            Products = _mapper.Map<ObservableCollection<ProductViewModel>>(_domainContext.GetProducts());
        }

        public ProductViewModel SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(nameof(SelectedProduct));
            }
        }

        public ObservableCollection<CategoryViewModel> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public ObservableCollection<ContractViewModel> Contracts
        {
            get
            {
                return _contracts;
            }
            set
            {
                _contracts = value;
                OnPropertyChanged(nameof(Contracts));
            }
        }

        public ObservableCollection<ProductViewModel> Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
                _products.CollectionChanged += ProductsOnCollectionChanged;
                OnPropertyChanged(nameof(Products));
            }
        }

        private ObservableCollection<ProductViewModel> _newProducts;
       
        private void ProductsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                foreach (ProductViewModel item in e.NewItems)
                {
                        _newProducts.Add(item);
                }

            if (e.OldItems != null)
                foreach (ProductViewModel item in e.OldItems)
                {
                    _newProducts.Remove(item);
                }
        }


        private RelayCommand _deleteCommand;
        private RelayCommand _updateCommand;
        private RelayCommand _addProductsCommand;


        public ICommand AddProductsCommand
        {
            get => _addProductsCommand ?? new RelayCommand(ExecuteAddProductsCommand, null);
        }

        public ICommand DeleteCommand
        {
            get => _deleteCommand ?? new RelayCommand(ExecuteDeleteCommand, null);
        }

        public ICommand UpdateCommand
        {
            get => _updateCommand ?? new RelayCommand(ExecuteUpdateCommand, null);
        }

        private void ExecuteUpdateCommand(object obj)
        {     
            _domainContext.UpdateProduct(_mapper.Map<Product>(SelectedProduct));
        }

        private void ExecuteDeleteCommand(object obj)
        {
            _domainContext.DeleteProduct(SelectedProduct.Id);
            Products.Remove(SelectedProduct);
        }


        private void ExecuteAddProductsCommand(object obj)
        {

            foreach (var product in _newProducts)
            {
                product.Category.Id = Categories.First(x => x.Name == product.Category.Name).Id;

                _domainContext.AddNewProduct(_mapper.Map<Product>(product));
            }
        }

    }
}
