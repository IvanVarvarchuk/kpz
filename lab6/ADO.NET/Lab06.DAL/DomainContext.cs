﻿using System;
using System.Data.SqlClient;

using Lab06.DomainLogic.Models;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;

namespace Lab06.DomainLogic
{
    public class DomainContext
    {
        private string _connectionString;

        public DomainContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        #region GetAll
        public ObservableCollection<Category> GetCategories()
        {
            ObservableCollection<Category> res = new ObservableCollection<Category>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Id, Name from Categories", connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        res.Add(new Category() 
                        {
                            Id = (int)reader.GetValue(0),
                            Name = (string)reader.GetValue(1)
                        });
                    }
                }
            }
            return res;
        } 
        
        public ObservableCollection<Product> GetProducts()
        {
            ObservableCollection<Product> res = new ObservableCollection<Product>();
            ObservableCollection<Category> categories = GetCategories();


            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Id, Name, Price, CategoryId from Products", connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        res.Add(new Product()
                        {
                            Id = (int)reader.GetValue(0),
                            Name = (string)reader.GetValue(1),
                            Price = (double)reader.GetValue(2),
                            Category = categories.First(x => x.Id == (int)reader.GetValue(3))
                        });
                    }
                }
            }

            return res;
        }

        private IEnumerable<ContractItem> GetContractItems() 
        {
            var res = new List<ContractItem>();
            ObservableCollection<Product> products = GetProducts();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand($"Select Id, ProductId, Quantity, ContractId from ContractItems", connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        res.Add(new ContractItem()
                        {
                            Id = (int)reader.GetValue(0),
                            Product = products.First(x => x.Id == (int)reader.GetValue(1)),
                            Quantity = (int)reader.GetValue(2),
                            ContractId = (int)reader.GetValue(3)
                        });
                    }
                } 
            }
            return res;
        }
        
        public ObservableCollection<Contract> GetContracts()
        {

            ObservableCollection<Contract> res = new ObservableCollection<Contract>();
            IEnumerable<ContractItem> contractItems = GetContractItems();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Id, ConclusionDate  from Contracts", connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        res.Add(new Contract()
                        {
                            Id = (int)reader.GetValue(0),
                            ConclusionDate = (DateTime)reader.GetValue(1),
                            ContractItems = contractItems.Where(c => c.ContractId == (int)reader.GetValue(0))
                        });
                    }
                }
            }

            return res;
        }
        #endregion

        public void DeleteProduct(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand($"delete from Products where Id = {id}", connection);
                command.ExecuteNonQuery();
            }
        }

        public void UpdateProduct(Product p)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand($"update Products set Name = '{p.Name}', Price = {(int)p.Price}, CategoryId = (select Id from Categories where Name = '{p.Category.Name}') where Id = {p.Id}", connection);
                command.ExecuteNonQuery();
            }
        }

        public void AddNewProduct(Product p)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("insert into Products(Name, Price, CategoryId) " +
                $"values ('{p.Name}', {p.Price}, {p.Category.Id})", connection);
                command.ExecuteNonQuery();
            }
        }
    }
}
