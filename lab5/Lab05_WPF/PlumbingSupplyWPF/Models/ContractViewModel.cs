﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class ContractViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private DateTime _conclusionDate;
        public DateTime ConclusionDate
        {
            get
            {
                return  _conclusionDate;
            }
            set
            {
                 _conclusionDate = value;
                OnPropertyChanged(nameof(ConclusionDate));
            }
        }

        private DateTime? _paymentDate;
        public DateTime? PaymentDate
        {
            get
            {
                return _paymentDate;
            }
            set
            {
                _paymentDate = value;
                OnPropertyChanged(nameof(PaymentDate));
            }
        }

        private DateTime? _completedDate;
        public DateTime? CompletedDate
        {
            get
            {
                return _completedDate;
            }
            set
            {
                _completedDate = value;
                OnPropertyChanged(nameof(CompletedDate));
            }
        }

        private ObservableCollection<ContractItemViewModel> _items;
        public ObservableCollection<ContractItemViewModel> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                OnPropertyChanged(nameof(Items));
            }
        }

        private WarehouseViewModel _warehouse;
        public WarehouseViewModel Warehose
        {
            get
            {
                return _warehouse;
            }
            set
            {
                _warehouse = value;
                OnPropertyChanged(nameof(Warehose));
            }
        }


        private int _totalSum;
        public int TotalSum
        {
            get
            {
                return _totalSum;
            }
            set
            {
                _totalSum = value;
                OnPropertyChanged(nameof(TotalSum));
            }
        }

    }
}
