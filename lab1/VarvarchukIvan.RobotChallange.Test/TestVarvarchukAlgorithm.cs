﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace VarvarchukIvan.RobotChallange.Test
{
    [TestClass]
    public class TestVarvarchukAlgorithm
    {
        [TestMethod]
        public void TestAttacStrategy ()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();
            
            VarvarchukIvanAlgorithm.RoundNumber = 31;
            
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot{ Energy = 100, OwnerName = "Ivan Varvarchuk", Position = new Position(1,3) },
                new Robot.Common.Robot{ Energy = 2000, OwnerName = "Ne_Lox_42", Position = new Position(2, 3) },
                new Robot.Common.Robot{ Energy = 3600, OwnerName = "Ne_Lox_42", Position = new Position(1, 6) }
            };


            //Act
            var command = algorithm.DoStep(robots, 0, new Map());


            //Assert
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(robots[2].Position, ((MoveCommand)command).NewPosition);

        }
       
        [TestMethod]
        public void TestMethodIsInSameSector ()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();
            
            var myRobot = new Robot.Common.Robot { OwnerName = "Ivan Varvarchuk", Position = new Position(1, 3) };
            var another = new Robot.Common.Robot { Energy = 2000, OwnerName = "Ne_Lox_42", Position = new Position(2, 3) };

            //Act
            bool result = myRobot.Position.IsInSameSector(another.Position);


            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestMethodFindEnemiesAround ()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();

            var targetPosition = new Position(5, 5);
            
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot{ Energy = 100, OwnerName = "Ivan Varvarchuk", Position = new Position(6,5) },
                new Robot.Common.Robot{ Energy = 200, OwnerName = "Ne_Lox_42", Position = new Position(4,4) },
                new Robot.Common.Robot{ Energy = 240, OwnerName = "Ne_Lox_42", Position = new Position(3,4) },
                new Robot.Common.Robot{ Energy = 360, OwnerName = "Ne_Lox_42", Position = new Position(6, 6) }
            };


            //Act
            var enemies = DistanceHelper.FindEnemiesAround(targetPosition, robots);


            //Assert
            
            Assert.AreEqual(2, enemies.Count);

        }
        
        [TestMethod]
        public void TestMethodFindOptimalStation()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();

            Map map = new Map { Stations =

                new List<EnergyStation>()
                {
                    new EnergyStation{ Energy = 100, Position = new Position(6,5) },
                    new EnergyStation{ Energy = 200, Position = new Position(4,4) },
                    new EnergyStation{ Energy = 240, Position = new Position(6,4) },
                    new EnergyStation{ Energy = 360, Position = new Position(6, 6) }
                }
            };

            var robot = new Robot.Common.Robot { 
                Energy = 600,
                OwnerName = "Ivan Varvarchuk",
                Position = new Position(7, 5) 
            };


            
            //Act
            var result = DistanceHelper.FindOptimalStation(robot, map);


            //Assert
            
            Assert.AreEqual(map.Stations.First().Position, result);

        }
        
        [TestMethod]
        public void TestMethodCalculateHop()
        {
            //Assign

            var station = new EnergyStation { Energy = 360, Position = new Position(8, 9) };

            var robot = new Robot.Common.Robot { 
                Energy = 200,
                OwnerName = "Ivan Varvarchuk",
                Position = new Position(1, 2) 
            };
            
            //Act
            var result = DistanceHelper.CalculateHop(robot, station.Position);


            //Assert
            
            Assert.AreEqual(new Position(4,5), result);

        }

        [TestMethod]
        public void TestIsOptimalChoose()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();

            Map map = new Map
            {
                Stations =

                new List<EnergyStation>()
                {
                    new EnergyStation{ Energy = 100, Position = new Position(6,5) },
                    new EnergyStation{ Energy = 200, Position = new Position(4,4) },
                    new EnergyStation{ Energy = 240, Position = new Position(6,4) },
                    new EnergyStation{ Energy = 360, Position = new Position(6, 6) },
                    new EnergyStation{ Energy = 360, Position = new Position(10, 3) }
                }
            };


            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot{ Energy = 100, OwnerName = "Ivan Varvarchuk", Position = new Position(10,5) },
                new Robot.Common.Robot{ Energy = 200, OwnerName = "Ne_Lox_42", Position = new Position(4,4) },
                new Robot.Common.Robot{ Energy = 240, OwnerName = "Ne_Lox_42", Position = new Position(3,4) },
                new Robot.Common.Robot{ Energy = 360, OwnerName = "Ne_Lox_42", Position = new Position(6, 6) }
            };

            //Act
            var result = algorithm.DoStep(robots, 0, map);


            //Assert
            Assert.IsTrue(result is MoveCommand);
            Assert.AreEqual(map.Stations.First().Position, ((MoveCommand)result).NewPosition);

        }
        
        [TestMethod]
        public void TestIfNoFreeStation()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();

            Map map = new Map
            {
                Stations =

                new List<EnergyStation>()
                {
                    new EnergyStation{ Energy = 100, Position = new Position(6,5) },
                    new EnergyStation{ Energy = 200, Position = new Position(4,4) },
                    new EnergyStation{ Energy = 240, Position = new Position(8,4) },
                }
            };


            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot{ Energy = 100, OwnerName = "Ivan Varvarchuk", Position = new Position(10,5) },
                new Robot.Common.Robot{ Energy = 200, OwnerName = "Ne_Lox_42", Position = new Position(6,4) },
                new Robot.Common.Robot{ Energy = 240, OwnerName = "Ne_Lox_42", Position = new Position(6,5) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ne_Lox_42", Position = new Position(3,4) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ne_Lox_42", Position = new Position(4,4) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ne_Lox_42", Position = new Position(8,4) },
                new Robot.Common.Robot{ Energy = 160, OwnerName = "Ne_Lox_42", Position = new Position(7, 4) }
            };

            //Act
            var result = algorithm.DoStep(robots, 0, map);


            //Assert
            Assert.IsTrue(result is MoveCommand);
            Assert.AreEqual(new Position(8,4), ((MoveCommand)result).NewPosition);

        }
        
        [TestMethod]
        public void TestMethodIsSectorOvercrowded()
        {
            //Assign
            
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot{ Energy = 200, OwnerName = "Ivan Varvarchuk", Position = new Position(5,3) },
                new Robot.Common.Robot{ Energy = 240, OwnerName = "Ivan Varvarchuk", Position = new Position(4,3) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ivan Varvarchuk", Position = new Position(5,4) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ivan Varvarchuk", Position = new Position(4,4) },
                new Robot.Common.Robot{ Energy = 160, OwnerName = "Ivan Varvarchuk", Position = new Position(5,5) }
            };

            //Act
            var result = robots[0].Position.IsSectorOvercrowded(robots);
            var result_ = robots[4].Position.IsSectorOvercrowded(robots);


            //Assert
            Assert.IsTrue(result);
            Assert.IsFalse(result_);

        }
        
        [TestMethod]
        public void TestWhichStationChoose()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();

            Map map = new Map
            {
                Stations =

                new List<EnergyStation>()
                {
                    new EnergyStation{ Energy = 100, Position = new Position(6,5) },
                    new EnergyStation{ Energy = 200, Position = new Position(4,4) },
                    new EnergyStation{ Energy = 240, Position = new Position(10,5) },
                }
            };


            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot{ Energy = 1500, OwnerName = "Ivan Varvarchuk", Position = new Position(5,5) },
                new Robot.Common.Robot{ Energy = 240, OwnerName = "Ne_Lox_42", Position = new Position(6,5) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ne_Lox_42", Position = new Position(3,4) },
                new Robot.Common.Robot{ Energy = 140, OwnerName = "Ne_Lox_42", Position = new Position(4,4) },
            };

            //Act
            var result = algorithm.DoStep(robots, 0, map);


            //Assert
            Assert.IsTrue(result is MoveCommand);
            Assert.AreEqual(new Position(10, 5), ((MoveCommand)result).NewPosition);

        }
        
        [TestMethod]
        public void TestMethodTotalEnergyLoss()
        {
            //Assign
            var algorithm = new VarvarchukIvanAlgorithm();


            var station = new EnergyStation { Energy = 240, Position = new Position(2, 4) };


            var robot = new Robot.Common.Robot { Energy = 100, OwnerName = "Ivan Varvarchuk", Position = new Position(10, 5) };

            //Act
            var result = DistanceHelper.TotalEnergyLoss(robot, station.Position);


            //Assert
            
            Assert.AreEqual(17, result);

        }
        
    }
}
