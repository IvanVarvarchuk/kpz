﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PlumbingSupply.BLL.DataTransferObjects
{
    public class ContractDTO
    {
        public int Id { get; set; }

        public DateTime ConclusionDate { get; set; }

        public DateTime? PaymentDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public ObservableCollection<ContractItemDTO> Items { get; set; }

        public WarehouseDTO Warehose { get; set; }

        public double TotalSum { get => Items.Sum(i => i.Quantity * i.Product.Price); }
    }
}
