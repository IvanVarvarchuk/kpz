﻿using AutoMapper;
using Lab07.DAL.Models;
using Lab07.DAL.Repositories;
using Lab07_WebAPI.ViewModels.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Lab07_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IRepository<Product> _productRepository;
        private IMapper _mapper;

        public ProductController(IRepository<Product> productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;

        }

        /// <summary>
        /// Returns all products.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductViewModel>>> GetFuels()
        {
            var products = await _productRepository.GetAllAsync();
            var productViewModel = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

            return Ok(productViewModel);
        }


        /// <summary>
        /// Returns product by id.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductViewModel>> GetProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var product = await _productRepository.GetAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            var productViewModel = _mapper.Map<Product, ProductViewModel>(product);
            return Ok(productViewModel);
        }


        /// <summary>
        /// Updates product by id.
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> PutProduct(UpdateProductViewModel product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateProduct = _mapper.Map<UpdateProductViewModel, Product>(product);

            var updatedProduct = await _productRepository.UpdateAsync(updateProduct);
            var updatedProductViewModel = _mapper.Map<Product, ProductViewModel>(updatedProduct);

            return Ok(updatedProductViewModel);
        }


        /// <summary>
        /// Create a Product.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductViewModel>> PostProduct(CreateProductViewModel createProductViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createProduct = _mapper.Map<CreateProductViewModel, Product>(createProductViewModel);
            var createdProduct = await _productRepository.AddAsync(createProduct);
            var createdProductViewModel = _mapper.Map<Product, ProductViewModel>(createdProduct);

            return Ok(createdProductViewModel);
        }


        /// <summary>
        /// Delete Product by id.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductViewModel>> DeleteProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _productRepository.DeleteAsync(id);
            return NoContent();
        }
    }
}
