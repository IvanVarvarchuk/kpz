﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab07_WebAPI.ViewModels
{
    public class ContractViewModel
    {
        public int Id { get; set; }
        public DateTime ConclusionDate { get; set; }
    }
}
