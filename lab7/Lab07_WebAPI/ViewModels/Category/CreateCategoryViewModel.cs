﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab07_WebAPI.ViewModels
{
    public class CreateCategoryViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }
    }
}
