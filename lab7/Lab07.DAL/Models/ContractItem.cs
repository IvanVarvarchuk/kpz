﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lab07.DAL.Models
{
    public partial class ContractItem
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int Quantity { get; set; }
        public int? ContractId { get; set; }

        public virtual Contract Contract { get; set; }
        public virtual Product Product { get; set; }
    }
}
