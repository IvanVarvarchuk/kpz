﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VarvarchukIvan.RobotChallange
{
    public static class DistanceHelper
    {
        public static int FindDistance(Position a, Position b) => (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        
        public static bool IsInSameSector(this Position position, Position target) 
        {
            return position.X >= target.X - 1 && position.X <= target.X + 1 &&
                   position.Y >= target.Y - 1 && position.Y <= target.Y + 1;
        }
        
        public static Position FindNearestOccupiedStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {

            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            var stations = map.Stations.Where(s => FindEnemiesAround(s.Position, robots).Count < 3)
                                       .Where(s=>!s.Position.IsSectorOvercrowded(robots));
            foreach (var station in map.Stations)
            {
         
                    int d = FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                
            }
            return nearest == null ? null : nearest.Position;
        }
        
        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            var stations = map.Stations.Where(s => FindEnemiesAround(s.Position, robots).Count < 3);
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        
        public static IList<Robot.Common.Robot> FindEnemiesAround(Position stationPosition, IList<Robot.Common.Robot> robots)
        {
            return robots.Where(r => r.OwnerName != "Ivan Varvarchuk")
                .Where(r => r.Position.IsInSameSector(stationPosition)).ToList();
        }
        
        public static Position FindRichestEnemy(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            var targets = robots.Where(r => r.OwnerName != movingRobot.OwnerName)
                                .Where(r => r.Energy * 0.05 > 50 + FindDistance(r.Position, movingRobot.Position));          
            
            return targets.Any()? targets.OrderBy(r => r.Energy).Last().Position : null;
        }
        
        public static int FindStationsCountAround(Position stationPosition, IList<EnergyStation> otherStations)
        {
            return otherStations.Where(s => s.Position != stationPosition)
                                .Where(s => stationPosition.IsInSameSector(s.Position))
                                .Count();
        }
        
        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return station.Position.IsCellFree(movingRobot, robots) ;
        }
        
        public static bool IsCellFree( this Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {

            foreach (var robot in robots)
            {
                if (robot != movingRobot && robot.Position == cell)
                {
                    return false;
                }
            }
            return true;
        }
        
        public static Position FindOptimalStation(Robot.Common.Robot movingRobot, Map map) 
        {
            var optimal = map.Stations.Where(s => FindStationsCountAround(s.Position, map.Stations) >= 2).ToList();

            if (optimal.Any())
            {
                var minDistance = optimal.Min(o => FindDistance(movingRobot.Position, o.Position));
                return optimal.Find(o => FindDistance(movingRobot.Position, o.Position) == minDistance).Position;
            }
            else
            {
                return null;
            }
        }

        public static bool IsSectorOvercrowded(this Position position, IList<Robot.Common.Robot> robots)
            => robots.Where(r => r.Position
            .IsInSameSector(position)).ToList()
            .Count(r => r.OwnerName == "Ivan Varvarchuk") > 3 ;

        private static Position MidlePoint(Position from, Position to) => 
            new Position { X = (from.X + to.X) / 2, Y = (from.Y + to.Y) / 2 };


        public static int TotalEnergyLoss(Robot.Common.Robot movingRobot, Position target, int totalEnergy = 0) 
        {
            

            if (FindDistance(movingRobot.Position, target) < movingRobot.Energy * 0.2)
            {
                return FindDistance(movingRobot.Position, target);
            }
            else 
            { 
                return totalEnergy += TotalEnergyLoss(movingRobot, MidlePoint(movingRobot.Position, target), totalEnergy);
            }
        }

        public static Position CalculateHop(Robot.Common.Robot movingRobot,  Position target)
        {
            if (FindDistance(movingRobot.Position, target) < movingRobot.Energy * 0.2)
            {
                FindDistance(movingRobot.Position, target);
                return target;
            }
            else
                return CalculateHop(movingRobot, MidlePoint(movingRobot.Position, target));
        }
    }
}
