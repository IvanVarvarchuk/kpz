﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using PlumbingSupplyWPF.Command;
using System.Windows.Input;

using System.Linq;
using AutoMapper;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupplyWPF.BLL.Services;
using System.Collections.Specialized;

namespace PlumbingSupplyWPF.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        private IMapper _mapper;
        private DataService _model;

        public DataViewModel(DataService dataService = null) {
            _model = dataService?? new DataService();
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CategoryViewModel, CategoryDTO>();
                cfg.CreateMap<CategoryDTO, CategoryViewModel>();

                cfg.CreateMap<ContractViewModel, ContractDTO>();
                cfg.CreateMap<ContractDTO, ContractViewModel>();

                cfg.CreateMap<ContractItemViewModel, ContractItemDTO>();
                cfg.CreateMap<ContractItemDTO, ContractItemViewModel>();

                cfg.CreateMap<DeliveryViewModel, DeliveryDTO>();
                cfg.CreateMap<DeliveryDTO, DeliveryViewModel>();

                cfg.CreateMap<LocationViewModel, LocationDTO>();
                cfg.CreateMap<LocationDTO, LocationViewModel>();

                cfg.CreateMap<ProductViewModel, ProductDTO>();
                cfg.CreateMap<ProductDTO, ProductViewModel>();

                cfg.CreateMap<SupplierViewModel, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, SupplierViewModel>();

                cfg.CreateMap<WarehouseViewModel, WarehouseDTO>();
                cfg.CreateMap<WarehouseDTO, WarehouseViewModel>();
            }));
            NewProducts = new ObservableCollection<ProductViewModel>();
            Products = new ObservableCollection<ProductViewModel>();
            Categories = new ObservableCollection<CategoryViewModel>();
            Suppliers = new ObservableCollection<SupplierViewModel>();
            Warehouses = new ObservableCollection<WarehouseViewModel>();
            LoadData();

        }

        private void LoadData()
        {
            var products = _model.GetAllProducts();
            foreach (var item in products)
            {
                Products.Add(_mapper.Map<ProductViewModel>(item));
            }

            foreach (var item in _model.GetAllCategories())
            {
                Categories.Add(_mapper.Map<CategoryViewModel>(item));
            }
            
            foreach (var item in _model.GetAllSuppliers())
            {
                Suppliers.Add(_mapper.Map<SupplierViewModel>(item));
            }
            
            foreach (var item in _model.GetWarehouses())
            {
                Warehouses.Add(_mapper.Map<WarehouseViewModel>(item));
            }

            foreach (var item in _model.GetAllContracts())
            {
                Contracts.Add(_mapper.Map<ContractViewModel>(item));
            }
        }


        #region Properties
        private ObservableCollection<ProductViewModel> _products;
        public ObservableCollection<ProductViewModel> Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
                OnPropertyChanged(nameof(Products));
            }
        }

        private ObservableCollection<CategoryViewModel> _categories;
        public ObservableCollection<CategoryViewModel> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        private ObservableCollection<SupplierViewModel> _suppliers;
        public ObservableCollection<SupplierViewModel> Suppliers
        {
            get
            {
                return _suppliers;
            }
            set
            {
                _suppliers = value;
                OnPropertyChanged(nameof(Suppliers));
            }
        }

        private ObservableCollection<WarehouseViewModel> _warehouse;
        public ObservableCollection<WarehouseViewModel> Warehouses
        {
            get
            {
                return _warehouse;
            }
            set
            {
                _warehouse = value;
                OnPropertyChanged(nameof(Warehouses));
            }
        }

        private ProductViewModel _selectedProduct;
        public ProductViewModel SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                if (_selectedProduct == null)
                {
                    _selectedProduct = new ProductViewModel();
                    
                }
                _selectedProduct = value;
                OnPropertyChanged(nameof(SelectedProduct));
            }
        }
       
        private ObservableCollection<ProductViewModel> _newProducts;
        private ObservableCollection<ContractViewModel> _contracts;
        public ObservableCollection<ContractViewModel> Contracts
        {
            get
            {
                return _contracts;
            }
            set
            {
                _contracts = value;
                OnPropertyChanged(nameof(Contracts));
            }
        }

        private ObservableCollection<ProductViewModel> NewProducts 
        {
            get 
            {
                return _newProducts;
            }
            set 
            {
                _newProducts = value;
                OnPropertyChanged(nameof(NewProducts));
            }
        }
        #endregion

        
        #region Comands
        private RelayCommand _deleteCommand;
        private RelayCommand _updateCommand;
        private RelayCommand _addProductsCommand;


        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(ExecuteDeleteCommand, x =>  SelectedProduct != null);
                }

                return _deleteCommand;
            }
        }
        public ICommand UpdateCommand
        {
            get
            {
                if (_updateCommand == null)
                {
                    _updateCommand = new RelayCommand(ExecuteUpdateCommand, x => SelectedProduct != null && SelectedProduct.Id != 0);
                }

                return _updateCommand;
            }
        }
        public ICommand AddProductsCommand
        {
            get
            {
                if (_addProductsCommand == null)
                {
                    _addProductsCommand = new RelayCommand(ExecuteAddProductsCommand, x=> SelectedProduct != null);
                }

                return _addProductsCommand;
            }
        }
        #endregion  
        


        private void ExecuteAddProductsCommand(object obj)
        {
            SelectedProduct.Category = Categories.First(x => x.Name == SelectedProduct.Category.Name);
            SelectedProduct.Supplier = Suppliers.First(x => x.CompanyName == SelectedProduct.Supplier.CompanyName);
            var item = _mapper.Map<ProductDTO>(SelectedProduct);
            _model.Create(item);
            
        }

        private void ExecuteUpdateCommand(object obj)
        {
            SelectedProduct.Category = Categories.First(x => x.Name == SelectedProduct.Category.Name);
            SelectedProduct.Supplier = Suppliers.First(x => x.CompanyName == SelectedProduct.Supplier.CompanyName);
            _model.Update(SelectedProduct.Id, _mapper.Map<ProductDTO>(SelectedProduct));
        }

        private void ExecuteDeleteCommand(object obj)
        {
            _model.Delete(_mapper.Map<ProductDTO>(SelectedProduct));
            Products.Remove(SelectedProduct);
        }
        /**/

    }
}
