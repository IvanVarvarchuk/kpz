﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Lab07.DAL.Models
{
    public partial class PlumbingSupplyContext : DbContext
    {
        public PlumbingSupplyContext()
        {
        }

        public PlumbingSupplyContext(DbContextOptions<PlumbingSupplyContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ContractItem> ContractItems { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Data Source=IVANVARVARCHUK;Initial Catalog=PlumbingSupply2;Integrated Security=True");
            }
        }

    }  
}
