﻿using Lab04_Classes.TestPolymorphism;
using PlumbingStoreSupply;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04_Classes
{
    class Program
    {

        const int count = 10000000;


        static void Main(string[] args)
        {

            long memory = Process.GetCurrentProcess().PrivateMemorySize64;

            var watch = Stopwatch.StartNew();

            object[] arr = new BaseClass[count];

            Console.WriteLine("create simple A object");

            for (int i = 0; i < count; i++)
            {
                arr[i] = new AClass();
            }
            watch.Stop();
            var elapssedMs = watch.ElapsedMilliseconds;
            long TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine($"1. Createtion {count} classes, time - {elapssedMs} ms, memory used - {0} kB", (TempMemory - memory) / 1000);
            memory = TempMemory;
            //У циклі відповідно до типу викликайте метод.
            Console.WriteLine("Invoke methods from A");
            watch = System.Diagnostics.Stopwatch.StartNew();
            foreach (object m in arr)
            {
                ((BaseClass)m).DoSmth(3);
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;
            Console.WriteLine("create B");

            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                arr[i] = new BClass();
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. Createtion {0} classes, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            //У циклі відповідно до типу викликайте метод.
            Console.WriteLine("Invoke methods from B");

            watch = Stopwatch.StartNew();
            foreach (object m in arr)
            {
                ((BaseClass)m).DoSmth((string)"test");
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            Console.WriteLine("create without inheritance");

            A[] arr1 = new A[count];

            watch = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                arr1[i] = new A();
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. Createtion {0} classes, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;
            //У циклі відповідно до типу викликайте метод.
            Console.WriteLine("Invoke A");

            watch = Stopwatch.StartNew();
            foreach (A m in arr1)
            {
                m.DoSmth((int)3);
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            Console.WriteLine("create");

            B[] arr2 = new B[count];

            watch = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                arr2[i] = new B();
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. Createtion {0} classes, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            Console.WriteLine("Invoke");

            watch = Stopwatch.StartNew();
            foreach (object m in arr2)
            {
                ((B)m).DoSmth((string)"test");
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;


            Console.WriteLine("create using interfaces");

            IBase[] arr3 = new IBase[count];

            watch = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                arr3[i] = new First();
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. Createtion {0} classes, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            Console.WriteLine("Invoke");

            watch = Stopwatch.StartNew();
            foreach (var m in arr3)
            {
                m.DoSmth((int)3);
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;


            Console.WriteLine("create");

            watch = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                arr3[i] = new Second();
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. Createtion {0} classes, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            Console.WriteLine("Invoke");

            watch = Stopwatch.StartNew();
            foreach (var m in arr3)
            {
                m.DoSmth((string)"test");
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            ReflectionClass[] arr4 = new ReflectionClass[count];


            Console.WriteLine("create using reflection");

            watch = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
                arr4[i] = new ReflectionClass();
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. Createtion {0} classes, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);
            memory = TempMemory;

            Console.WriteLine("Invoke");

            watch = Stopwatch.StartNew();
            foreach (object m in arr4)
            {
                ((ReflectionClass)m).DoSmth((int)3);
            }
            watch.Stop();
            elapssedMs = watch.ElapsedMilliseconds;
            TempMemory = Process.GetCurrentProcess().PrivateMemorySize64;

            Console.WriteLine("1. invoked {0} methods, time - {1} ms, memory used - {2} kB", count, elapssedMs, (TempMemory - memory) / 1000);

            Console.ReadLine();
        }

        
    }
}
