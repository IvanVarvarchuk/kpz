﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumbingStoreSupply
{
    public class Product : IComparable<Product>
    {
        public Product() { }

        public Product(string name, ProductCategory category, int price, Supplier supplier)
        {
            Name = name;
            Price = price;
            Category = category;
            Supplier = supplier;
        }
        
        public string Name { get; set; }
        
        public int Price { get; set; }
        
        public ProductCategory Category { get; set; }
        
        public Supplier Supplier { get; set; }

        public int CompareTo(Product product)
        {
            return this.Price.CompareTo(product.Price);
        }
    }
}
