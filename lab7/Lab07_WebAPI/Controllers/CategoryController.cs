﻿using AutoMapper;
using Lab07.DAL.Models;
using Lab07.DAL.Repositories;
using Lab07_WebAPI.ViewModels;
using Lab07_WebAPI.ViewModels.Category;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Lab07_WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        private IRepository<Category> _categoryRepository;
        private IMapper _mapper;

        public CategoryController(IRepository<Category> categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;

        }

        /// <summary>
        /// Returns all categories.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryViewModel>>> GetFuels()
        {
            var categories = await _categoryRepository.GetAllAsync();
            var categoryViewModel = _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(categories);

            return Ok(categoryViewModel);
        }


        /// <summary>
        /// Returns category by id.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryViewModel>> GetCategory(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var category = await _categoryRepository.GetAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            var categoryViewModel = _mapper.Map<Category, CategoryViewModel>(category);
            return Ok(categoryViewModel);
        }


        /// <summary>
        /// Updates category by id.
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> PutCategory(UpdateCategoryViewModel category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateCategory = _mapper.Map<UpdateCategoryViewModel, Category>(category);

            var updatedCategory = await _categoryRepository.UpdateAsync(updateCategory);
            var updatedCategoryViewModel = _mapper.Map<Category, CategoryViewModel>(updatedCategory);

            return Ok(updatedCategoryViewModel);
        }

        /// <summary>
        /// Create a Category.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CategoryViewModel>> PostProduct(CreateCategoryViewModel createCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createCategory = _mapper.Map<CreateCategoryViewModel, Category>(createCategoryViewModel);
            var createdCategory = await _categoryRepository.AddAsync(createCategory);
            var createdCategoryViewModel = _mapper.Map<Category, CategoryViewModel>(createdCategory);

            return Ok(createdCategoryViewModel);
        }


        /// <summary>
        /// Delete Category by id.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<CategoryViewModel>> DeleteProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _categoryRepository.DeleteAsync(id);
            return NoContent();
        }

    }
}
