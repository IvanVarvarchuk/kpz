﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lab06.DomainLogic.Models
{
    public class ContractItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int ContractId { get; set; }
        public Product Product { get; set; }
    }
}
