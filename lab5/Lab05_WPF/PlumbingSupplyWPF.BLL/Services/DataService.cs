﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupply.Domain.Models;
using PlumbingSupply.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace PlumbingSupplyWPF.BLL.Services
{
    public class DataService 
    {
        PlumbingSupplyDBContextFactory _contextFactory;
        private readonly IMapper _mapper;

        public DataService(PlumbingSupplyDBContextFactory contextFactory = null)
        {
            _contextFactory = contextFactory?? new PlumbingSupplyDBContextFactory();   
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Category, CategoryDTO>();
                cfg.CreateMap<CategoryDTO, Category>();
                
                cfg.CreateMap<Contract, ContractDTO>();
                cfg.CreateMap<ContractDTO, Contract>();
                
                cfg.CreateMap<ContractItem, ContractItemDTO>();
                cfg.CreateMap<ContractItemDTO, ContractItem>();
                
                cfg.CreateMap<Delivery, DeliveryDTO>();
                cfg.CreateMap<DeliveryDTO, Delivery>();

                cfg.CreateMap<Location, LocationDTO>();
                cfg.CreateMap<LocationDTO, Location>();
                
                cfg.CreateMap<Product, ProductDTO>();
                cfg.CreateMap<ProductDTO, Product>();

                cfg.CreateMap<Supplier, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, Supplier>();
                
                cfg.CreateMap<Warehouse, WarehouseDTO>();
                cfg.CreateMap<WarehouseDTO, Warehouse>();
            }));
        }

        public void Delete(ProductDTO entity)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                Product toRemove = _mapper.Map<Product>(entity);
                context.Products.Remove(context.Products.First(x => x.Id == toRemove.Id));
                context.SaveChanges();
            }
        }

        
        public IEnumerable<ProductDTO> GetAllProducts()
        {
            List<ProductDTO> res = new List<ProductDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                List<Product> products = context.Products
                                                .Include(x => x.Category)
                                                .Include(x => x.Supplier)
                                                .ToList();
                foreach (var item in products)
                {
                    res.Add(_mapper.Map<ProductDTO>(item));
                }
            }
            return res;
        }
        
        public IEnumerable<SupplierDTO> GetAllSuppliers()
        {
            List<SupplierDTO> res = new List<SupplierDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                List<Supplier> suppliers = context.Suppliers.Include(x => x.Location).ToList();
                foreach (var item in suppliers)
                {
                    res.Add(_mapper.Map<SupplierDTO>(item));
                }
            }
            return res;
        }

        public IEnumerable<ContractDTO> GetAllContracts()
        {
            List<ContractDTO> res = new List<ContractDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                List<Contract> contracts = context.Contracts
                                                  .Include(x => x.Items)
                                                  .ThenInclude(x => x.Product)
                                                  .ToList();
                foreach (var item in contracts)
                {
                    res.Add(_mapper.Map<ContractDTO>(item));
                }
            }
            return res;
        }

        public IEnumerable<DeliveryDTO> GetAllDeliveries() 
        {
            List<DeliveryDTO> res = new List<DeliveryDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                var deliveries = context.Deliveries
                    .Include(x => x.Product)
                    .Include(x => x.Warehouse)
                    .ToList();
                foreach (var item in deliveries)
                {
                    res.Add(_mapper.Map<DeliveryDTO>(item));
                }
            }
            return res;
        }

        public IEnumerable<CategoryDTO> GetAllCategories()
        {
            List<CategoryDTO> res = new List<CategoryDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                foreach (var item in context.Categories.ToList())
                {
                    res.Add(_mapper.Map<CategoryDTO>(item));
                }
            }
            return res;
        }

        public IEnumerable<WarehouseDTO> GetWarehouses() 
        {
            List<WarehouseDTO> res = new List<WarehouseDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                foreach (var item in context.Warehouses.Include(x=>x.Address).ToList())
                {
                    res.Add(_mapper.Map<WarehouseDTO>(item));
                }
            }
            return res;
        }

        public void Update(int id, ProductDTO product)
        {
            Product toUpdate = _mapper.Map<Product>(product);
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Entry(context.Products.First(x => x.Id == product.Id)).CurrentValues.SetValues(toUpdate);
                context.SaveChanges();
            }

        }

        

        public void Create(ProductDTO entity)
        {

            using (var context = _contextFactory.CreateDbContext())
            {

                var created = _mapper.Map<Product>(entity);
                created.Supplier = context.Suppliers.First(x=> x.CompanyName == created.Supplier.CompanyName);
                created.Category = context.Categories.First(x=> x.Name == created.Category.Name);
                context.Products.Add(created);
                context.SaveChanges();
            }
        }

       
    }
}
