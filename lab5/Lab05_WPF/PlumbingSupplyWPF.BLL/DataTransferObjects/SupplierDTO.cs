﻿namespace PlumbingSupply.BLL.DataTransferObjects
{
    public class SupplierDTO
    {
        public int Id { get; set; }
        public string CompaNyName { get; set; }
        public string Email { get; set; }
        public LocationDTO Location { get; set; }

    }
}