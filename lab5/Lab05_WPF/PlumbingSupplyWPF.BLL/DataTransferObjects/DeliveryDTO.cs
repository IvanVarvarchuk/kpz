﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlumbingSupply.BLL.DataTransferObjects
{
    public class DeliveryDTO
    {
        public int Id { get; set; }
        public WarehouseDTO Warehouse { get; set; }
        public ProductDTO Product { get; set; }
        public int Quantity { get; set; }
        public DateTime Date { get; set; }
    }
}
