﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumbingStoreSupply
{
    public interface IUserRole
    {
        public AccessLevel UserLevel { get;}
    }

    public enum AccessLevel { Supplier, Emploee , Accountant }

    abstract public class Person
    {
        public Person(string firstn, string lastn, Certification certification)
        {
            FirstName = firstn;
            LastName = lastn;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        protected Certification _certification;
    }


    public class Employee : Person , IUserRole
    {

        public Employee(string firstn, string lastn, Certification certification, DateTime startDate,
            int workingHours) : base (firstn, lastn, certification)
        {
            _statDate = startDate;
            WorkingHours = workingHours;
        }

        public int WorkingHours { get; set; }
        public DateTimeOffset WorkingExperience { get; set; }
        public AccessLevel UserLevel { get => AccessLevel.Emploee; }
        protected DateTime _statDate;
    }

    public class Accoutant : Employee
    {
        public Accoutant(string firstn, string lastn, Certification certification, DateTime startDate,
            int workingHours) : base(firstn, lastn, certification, startDate, workingHours) { }
        public AccessLevel UserLavel { get => AccessLevel.Accountant; }
    }
    
    /*public class SupplierAgent : Person , IUserRole
    {
        public SupplierAgent(string firstn, string lastn, Certification certification) :
            base(firstn, lastn, certification) { }

        public AccessLevel UserLevel { get => AccessLevel.Emploee; }
    }
*/

}
