﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumbingStoreSupply
{
    public class Supplier
    {
        public Supplier() {}

        public Supplier(int id, string company, string country, Person contactPerson)
        {
            Id = id;
            Company = company;
            Country = country;
            _Agent = (SupplierAgent)contactPerson;
        }

        public int Id { get; set; }

        public string Company { get; set; }
        
        public string Country { get; set; }
       
        private SupplierAgent _Agent; 
        public string Contact { get => _Agent.Contact; }
        
        private class SupplierAgent : Person, IUserRole
        {
            public SupplierAgent(string firstn, string lastn, Certification certification) :
                base(firstn, lastn, certification) { }

            public string Contact { get => _certification.Email; } 

            public AccessLevel UserLevel { get => AccessLevel.Supplier; }
        }

        public override string ToString() => $"Company:{Company}; From:{Country}; Contact{Contact}";
        
    }
}
