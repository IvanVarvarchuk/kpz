﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlumbingStoreSupply
{
    public class Contract
    {
        public Contract() { }

        public Contract(int id, DateTime concludedDate, DateTime paymentDate, List<ContractItem> contractItems)
        {
            Id = id;
            ConcludedDate = concludedDate;
            PaymentDate = paymentDate;
            ContractItems = contractItems;
            CurrentState = GetState();
        }

        private ContractState GetState()
        {
            if (this.CompletedDate != default) 
            {
                return ContractState.Payed;
            }
            else if (this.PaymentDate != default)
            {
                return ContractState.Payed;
            }
            return ContractState.Planned;
        }

        public int Id { get; set; }

        public DateTime ConcludedDate { get; set; }
        
        public DateTime PaymentDate { get; set; }
        
        public DateTime CompletedDate { get; set; }
        
        public List<ContractItem> ContractItems { get; set; }
        
        public ContractState CurrentState { get; set; }

        public int TotalSum
        {
            get
            {
                return ContractItems.Sum(n => n.Product.Price * n.Quantity);
            }
        }

        public bool IsActive() => (CurrentState & ContractState.Active) == CurrentState;
     
    }

    public enum ContractState {
        Planned,
        Payed,
        Completed,
        Canceled,
        Active = Planned | Payed
    }
}

