﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab07_WebAPI.ViewModels.Product
{
    public class CreateProductViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public int CategoryId { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Range(typeof(decimal), "0,00", "15000")]
        public decimal Price { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Range(typeof(int), "0", "10000")]
        public int Quantity { get; set; }

    }
}
