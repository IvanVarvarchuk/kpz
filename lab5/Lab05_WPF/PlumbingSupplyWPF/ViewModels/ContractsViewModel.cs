﻿using AutoMapper;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupplyWPF.BLL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class ContractsViewModel : ViewModelBase
    {

        private IMapper _mapper;
        private DataService _model;

        public ContractsViewModel(DataService dataService = null)
        {
            _model = dataService ?? new DataService();
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CategoryViewModel, CategoryDTO>();
                cfg.CreateMap<CategoryDTO, CategoryViewModel>();

                cfg.CreateMap<ContractViewModel, ContractDTO>();
                cfg.CreateMap<ContractDTO, ContractViewModel>();

                cfg.CreateMap<ContractItemViewModel, ContractItemDTO>();
                cfg.CreateMap<ContractItemDTO, ContractItemViewModel>();

                cfg.CreateMap<DeliveryViewModel, DeliveryDTO>();
                cfg.CreateMap<DeliveryDTO, DeliveryViewModel>();

                cfg.CreateMap<LocationViewModel, LocationDTO>();
                cfg.CreateMap<LocationDTO, LocationViewModel>();

                cfg.CreateMap<ProductViewModel, ProductDTO>();
                cfg.CreateMap<ProductDTO, ProductViewModel>();

                cfg.CreateMap<SupplierViewModel, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, SupplierViewModel>();

                cfg.CreateMap<WarehouseViewModel, WarehouseDTO>();
                cfg.CreateMap<WarehouseDTO, WarehouseViewModel>();
            }));
            
            Contracts = new ObservableCollection<ContractViewModel>();
            LoadData();

        }

        private void LoadData()
        {
            
            foreach (var item in _model.GetAllContracts())
            {
                Contracts.Add(_mapper.Map<ContractViewModel>(item));
            }
        }

        private ObservableCollection<ContractViewModel> _contracts;

        public ObservableCollection<ContractViewModel> Contracts
        {
            get
            {
                return _contracts;
            }
            set
            {
                _contracts = value;
                OnPropertyChanged(nameof(Contracts));
            }
        }
    }
}
