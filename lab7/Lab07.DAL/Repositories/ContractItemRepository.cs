﻿using Lab07.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lab07.DAL.Repositories
{
    public class ContractItemRepository : IRepository<ContractItem>
    {
        private PlumbingSupplyContext _context;
        public ContractItemRepository(PlumbingSupplyContext context)
        {
            _context = context;
        }

        public async Task<ContractItem> AddAsync(ContractItem entity)
        {
            await _context.ContractItems.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var deletedContractItem = await _context.ContractItems.FirstOrDefaultAsync(x => x.Id == id);
            _context.ContractItems.Remove(deletedContractItem);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ContractItem>> GetAllAsync()
        {
            var contractItems = await _context.ContractItems.Include(x => x.Contract).Include(x => x.Product).AsNoTracking().ToListAsync();
            return contractItems;
        }

        public async Task<ContractItem> GetAsync(int id)
        {
            var contract = await _context.ContractItems.Include(x => x.Contract).Include(x=>x.Product).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return contract;
        }

        public async Task<ContractItem> UpdateAsync(ContractItem entity)
        {
            var updatedContractItem = await _context.ContractItems.Include(x => x.Product).Include(x=>x.Contract).FirstOrDefaultAsync(x => x.Id == entity.Id);

            updatedContractItem.ContractId = entity.ContractId;
            updatedContractItem.Contract = entity.Contract;
            updatedContractItem.ProductId = entity.ProductId;
            updatedContractItem.Product = entity.Product;
            updatedContractItem.Quantity = entity.Quantity;

            await _context.SaveChangesAsync();
            return updatedContractItem;
        }
    }
}
