﻿namespace PlumbingSupply.BLL.DataTransferObjects
{
    public class ContractItemDTO
    {
        public int Id { get; set; }
        public ProductDTO Product { get; set; }
        public int Quantity { get; set; }

    }
}