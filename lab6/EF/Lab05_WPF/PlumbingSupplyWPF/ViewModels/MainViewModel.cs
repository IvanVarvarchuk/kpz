﻿using PlumbingSupplyWPF.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PlumbingSupplyWPF.ViewModels
{
    public class MainViewModel : ViewModelBase
    {           
        public ICommand UpdateViewModelCommand { get; set; }

        public MainViewModel()
        {
            UpdateViewModelCommand = new UpdateViewModelCommand(this);
        }

        private ViewModelBase _CurrentViewModel = new DataViewModel();

        public ViewModelBase CurrentViewModel
        {
            get 
            {
                return _CurrentViewModel; 
            }
            set 
            {
                _CurrentViewModel = value;
                OnPropertyChanged(nameof(CurrentViewModel));
            }
        }



    }
}
