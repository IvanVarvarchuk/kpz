﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlumbingStoreSupply;

namespace LinqTests
{
    [TestClass]
    public class PlumbingstoreTest
    {
        public List<Supplier> Suppliers {
            get
            {
                return new List<Supplier>() 
                {
                    new Supplier {Id = 001, Company = "Geberit",  Country = "German" },
                    new Supplier {Id = 002, Company = "Grohe",  Country = "German" },
                    new Supplier {Id = 003, Company = "Bossini",  Country = "Spain" },
                    new Supplier {Id = 004, Company = "Oras",  Country = "USA" },
                    new Supplier {Id = 005, Company = "Ravak",  Country = "Ukraine" },
                    new Supplier {Id = 006, Company = "Imprese",  Country = "UK" }
                };
            } 
        }
        
        public List<Product> Products {
            get
            {
                return new List<Product>() 
                {
                    new Product {CategoryID = 002, Name = "Product12", Price= 320, SupplierID = 001},
                    new Product {CategoryID = 003, Name = "Product2", Price= 2320, SupplierID = 004},
                    new Product {CategoryID = 001, Name = "Product3", Price= 190, SupplierID = 001},
                    new Product {CategoryID = 002, Name = "Product4", Price= 30, SupplierID = 001},
                    new Product {CategoryID = 003, Name = "Product5", Price= 720, SupplierID = 006},
                    new Product {CategoryID = 001, Name = "Product6", Price= 504, SupplierID = 003},
                    new Product {CategoryID = 002, Name = "Product7", Price= 3110, SupplierID = 005},
                    new Product {CategoryID = 002, Name = "Product8", Price= 230, SupplierID = 002}
                };
            } 
        }

        public List<ProductCategory> Categories
        {
            get
            {
                return new List<ProductCategory>()
                {
                    new ProductCategory{ Id = 001, Name = "Ceramics" },
                    new ProductCategory{ Id = 002, Name = "Boilers" },
                    new ProductCategory{ Id = 003, Name = "Pipes" }
                };
            }
        }

        [TestMethod]
        public void TestSorting()
        {
            var sorted = Products.ToArray();
            var comparer = Comparer<Product>.Create((p1, p2)=>p1.Name.CompareTo(p2.Name));
            
            Array.Sort(sorted, comparer);
            var ordered = Products.OrderByDescending(p=>p.Price).ToList();
            
            Assert.IsTrue(ordered.IsOrderedDescending() && sorted.IsOrdered(comparer)); 
        }
        
        [TestMethod]
        public void TestFiltring()
        {
            var onlyGermanSupply = from p in Products
                                from s in Suppliers
                                where s.Country == "German"
                                where p.SupplierID == s.Id
                                select p;
            
            Assert.IsTrue(onlyGermanSupply.Count() == 4); 
        }
        
        [TestMethod]
        public void TestSelecting()
        {
            Dictionary<Product, int> productsCounts = new Dictionary<Product, int>
            {
                [Products[0]] = 10,
                [Products[2]] = 12,
                [Products[1]] = 3,
                [Products[3]] = 6,
                [Products[6]] = 1
            };

            var contract = new Contract()
            {
                Id = 023,
                ConcludedDate = DateTime.Now,
                ContractItems = productsCounts
                    .Select((p)=> new ContractItem(p.Key, p.Value))
                    .ToList()
            };

            int totalSum = 0;
            foreach (var item in productsCounts)
            {
                totalSum += item.Key.Price * item.Value;
            }

            var productsFromCountries = from p in Products
                                        from s in Suppliers
                                        where p.SupplierID == s.Id
                                        select new {
                                            p.Name, 
                                            CountryFrom = s.Country
                                        };
            Assert.AreEqual(totalSum, contract.TotalSum); 
        }

        [TestMethod]
        public void TestGrouping()
        {
            var mostProducts = Products.GroupBy(p => p.CategoryID)
                .Select(g => new { Key = g.Key, Count = g.Count() })
                .Max(g => g.Count);

            Assert.AreEqual(4, mostProducts); 
        }
    }
}
