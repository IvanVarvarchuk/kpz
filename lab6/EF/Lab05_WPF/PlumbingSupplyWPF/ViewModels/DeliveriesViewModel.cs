﻿using AutoMapper;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupplyWPF.BLL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class DeliveriesViewModel : ViewModelBase
    {
        private IMapper _mapper;
        private DataService _model;

        public DeliveriesViewModel(DataService dataService = null)
        {
            _model = dataService ?? new DataService();
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                
                cfg.CreateMap<DeliveryViewModel, DeliveryDTO>();
                cfg.CreateMap<DeliveryDTO, DeliveryViewModel>();

                cfg.CreateMap<LocationViewModel, LocationDTO>();
                cfg.CreateMap<LocationDTO, LocationViewModel>();

                cfg.CreateMap<ProductViewModel, ProductDTO>();
                cfg.CreateMap<ProductDTO, ProductViewModel>();

                cfg.CreateMap<SupplierViewModel, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, SupplierViewModel>();

                cfg.CreateMap<WarehouseViewModel, WarehouseDTO>();
                cfg.CreateMap<WarehouseDTO, WarehouseViewModel>();
            }));
            Deliveries = new ObservableCollection<DeliveriesViewModel>(); 
            LoadData();

        }

        private ObservableCollection<DeliveriesViewModel> _deliveries;
        public ObservableCollection<DeliveriesViewModel> Deliveries
        {
            get
            {
                return _deliveries;
            }
            set
            {
                _deliveries = value;
                OnPropertyChanged(nameof(Deliveries));
            }
        }

        private void LoadData()
        {
            foreach (var item in _model.GetAllContracts())
            {
                Deliveries.Add(_mapper.Map<DeliveriesViewModel>(item));
            }
        }
    }
}
