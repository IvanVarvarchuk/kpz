﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PlumbingSupplyWPF.BLL.Services
{
    public interface IDataService<T> where T : class
    {
        IEnumerable<T> GetAll();
        void Create(T entity);
        void Update(int id, T entity);
        void Delete(T entity);

    }
}
