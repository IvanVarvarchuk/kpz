﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class ProductViewModel : ViewModelBase
    {


        public ProductViewModel()
        {
            Category = new CategoryViewModel();
            Supplier = new SupplierViewModel();
        }
        private int _id;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private double _price;
        public double Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                OnPropertyChanged(nameof(Price));
            }
        }

        private CategoryViewModel _category;
        public CategoryViewModel Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
                OnPropertyChanged(nameof(Category));
            }
        }

        private SupplierViewModel _supplier;
        public SupplierViewModel Supplier
        {
            get
            {
                return _supplier;
            }
            set
            {
                _supplier = value;
                OnPropertyChanged(nameof(Supplier));
            }
        }

    }
}
