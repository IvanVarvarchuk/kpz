﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class PersonViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private string _firstNamre;
        public string FirstNamre
        {
            get
            {
                return _firstNamre;
            }
            set
            {
                _firstNamre = value;
                OnPropertyChanged(nameof(FirstNamre));
            }
        }

        private string _secondNamre;
        public string SecondNamre
        {
            get
            {
                return _secondNamre;
            }
            set
            {
                _secondNamre = value;
                OnPropertyChanged(nameof(SecondNamre));
            }
        }

        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                OnPropertyChanged(nameof(Email));
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }


    }
}
