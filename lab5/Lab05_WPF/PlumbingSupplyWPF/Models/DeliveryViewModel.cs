﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlumbingSupplyWPF.ViewModels
{
    public class DeliveryViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private int _quantity;
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
                OnPropertyChanged(nameof(Quantity));
            }
        }

        private ProductViewModel _product;
        public ProductViewModel Product 
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;
                OnPropertyChanged(nameof(Product));
            }
        }


        private WarehouseViewModel _warehouse;
        public WarehouseViewModel Warehouse
        {
            get
            {
                return _warehouse;
            }
            set
            {
                _warehouse = value;
                OnPropertyChanged(nameof(Warehouse));
            }
        }

        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
                OnPropertyChanged(nameof(Date));
            }
        }

        
    }
}
