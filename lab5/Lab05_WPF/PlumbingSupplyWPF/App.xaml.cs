﻿using PlumbingSupply.EntityFramework;
using PlumbingSupplyWPF.BLL.Services;
using PlumbingSupplyWPF.ViewModels;
using PlumbingSupplyWPF.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace PlumbingSupplyWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //initialize the splash screen and set it as the application main window
            var splashScreen = new SplashScreenWindow();
            this.MainWindow = splashScreen;
            splashScreen.Show();
            //PlumbingSupplyDBContextFactory dbContextFactory = new PlumbingSupplyDBContextFactory();
            //DataService dataService = new DataService(dbContextFactory);
            //DataViewModel dataViewModel = new DataViewModel(dataService);
            
            Task.Factory.StartNew(() =>
            {
                for (int i = 1; i <= 100; i++)
                {
                    System.Threading.Thread.Sleep(30);

                    splashScreen.Dispatcher.Invoke(() => splashScreen.Progress = i);
                }
               
                this.Dispatcher.Invoke(() =>
                {

                    var mainWindow = new MainWindow(); //{ DataContext = dataViewModel };
                    this.MainWindow = mainWindow;
                    mainWindow.Show();
                    splashScreen.Close();
                });
            });
        }

    }
}
