﻿using Lab07.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lab07.DAL.Repositories
{
    public class ContractRepository : IRepository<Contract>
    {

        private PlumbingSupplyContext _context;
        public ContractRepository(PlumbingSupplyContext context)
        {
            _context = context;
        }

        public async Task<Contract> AddAsync(Contract entity)
        {
            await _context.Contracts.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var deletedContract = await _context.Contracts.FirstOrDefaultAsync(x => x.Id == id);
            _context.Contracts.Remove(deletedContract);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Contract>> GetAllAsync()
        {
            var contracts = await _context.Contracts.Include(x => x.ContractItems).AsNoTracking().ToListAsync();
            return contracts;
        }

        public async Task<Contract> GetAsync(int id)
        {
            var contract = await _context.Contracts.Include(x => x.ContractItems).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            return contract;
        }

        public async Task<Contract> UpdateAsync(Contract entity)
        {
            var updatedContract = await _context.Contracts.Include(x=>x.ContractItems).FirstOrDefaultAsync(x => x.Id == entity.Id);

            updatedContract.ConclusionDate = entity.ConclusionDate;
            updatedContract.ContractItems = entity.ContractItems;

            await _context.SaveChangesAsync();
            return updatedContract;
        }
    }
}
