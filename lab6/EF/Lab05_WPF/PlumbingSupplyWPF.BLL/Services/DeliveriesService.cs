﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlumbingSupply.BLL.DataTransferObjects;
using PlumbingSupply.Domain.Models;
using PlumbingSupply.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlumbingSupplyWPF.BLL.Services
{
    public class DeliveryService : IDataService<DeliveryDTO>
    {
        private readonly IMapper _mapper;
        private PlumbingSupplyDBContextFactory _contextFactory;

        public DeliveryService(PlumbingSupplyDBContextFactory contextFactory = null)
        {
            _contextFactory = contextFactory?? new PlumbingSupplyDBContextFactory();
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Delivery, DeliveryDTO>();
                cfg.CreateMap<DeliveryDTO, Delivery>();

                cfg.CreateMap<Location, LocationDTO>();
                cfg.CreateMap<LocationDTO, Location>();

                cfg.CreateMap<Product, ProductDTO>();
                cfg.CreateMap<ProductDTO, Product>();

                cfg.CreateMap<Supplier, SupplierDTO>();
                cfg.CreateMap<SupplierDTO, Supplier>();

                cfg.CreateMap<Warehouse, WarehouseDTO>();
                cfg.CreateMap<WarehouseDTO, Warehouse>();
            }));
        }


        public void Create(DeliveryDTO entity)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Deliveries.Add(_mapper.Map<Delivery>(entity));
                context.SaveChanges();
            }
        }

        public void Delete(DeliveryDTO entity)
        {
            Delivery toDelete = _mapper.Map<Delivery>(entity);
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Deliveries.Remove(toDelete);
                context.SaveChanges();
            }
        }

        public IEnumerable<DeliveryDTO> GetAll()
        {
            List<DeliveryDTO> res = new List<DeliveryDTO>();
            using (var context = _contextFactory.CreateDbContext())
            {
                var deliveries = context.Deliveries
                    .Include(x=>x.Product)
                    .Include(x=>x.Warehouse)
                    .ToList();
                foreach (var item in deliveries)
                {
                    res.Add(_mapper.Map<DeliveryDTO>(item));
                }
            }
            return res;
        }

        public void Update(int id, DeliveryDTO entity)
        {
            Delivery toUpdate = _mapper.Map<Delivery>(entity);
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Entry(context.Deliveries.First(x => x.Id == entity.Id))
                    .CurrentValues.SetValues(toUpdate);
                context.SaveChanges();
            }
        }
    }
}
