﻿namespace PlumbingSupply.BLL.DataTransferObjects
{
    public class WarehouseDTO
    {
        public int Id { get; set; }
        
        public LocationDTO Address { get; set; }
    }
}