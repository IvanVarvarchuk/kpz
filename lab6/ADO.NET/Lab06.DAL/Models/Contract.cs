﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lab06.DomainLogic.Models
{
    public class Contract
    {
        
        public int Id { get; set; }
        public DateTime ConclusionDate { get; set; }

        public IEnumerable<ContractItem> ContractItems { get; set; }
    }
}
