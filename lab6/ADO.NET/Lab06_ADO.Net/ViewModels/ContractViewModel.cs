﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Lab06_ADO.Net.ViewModels
{
    public class ContractViewModel : ProductViewModel
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        private ObservableCollection<ContractItemViewModel> _contractItems;
        public ObservableCollection<ContractItemViewModel> ContractItems
        {
            get
            {
                return _contractItems;
            }
            set
            {
                _contractItems = value;
                OnPropertyChanged(nameof(ContractItems));
            }
        }
    }
}
