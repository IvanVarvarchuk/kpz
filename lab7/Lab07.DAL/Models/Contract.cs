﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lab07.DAL.Models
{
    public partial class Contract
    {
        public Contract()
        {
            ContractItems = new HashSet<ContractItem>();
        }

        public int Id { get; set; }
        public DateTime ConclusionDate { get; set; }

        public virtual ICollection<ContractItem> ContractItems { get; set; }
    }
}
